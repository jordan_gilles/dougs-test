# Dougs Test

## Install:
```
$ npm install
$ npm run dev
```

Ouvrir le navigateur sur [http://localhost:8080](http://localhost:8080)

Cette API de validation vérifie l'intégrité de mouvements bancaires comparé à un solde bancaire fourni pour une période

En cas d'anomalie, la réponse comprend un message d'erreur global, un status code adapté ainsi qu'un tableau d'objets qui indique: 
- la raison de l'anomalie 
- la période vérifié par rapport aux soldes (date du dernier solde et du courant)
- la liste de tout les mouvements de cette période
- la différence entre le cumule des montants de la période et le solde indiqué
- et enfin le solde compris


Test avec un outil tel que Postman :
- url api : http://localhost:8080/movements/validation
- payload d'exemple : 
```json
{
    "movements": [
            { "id": 1, "date": "2021-06-10", "label": "CLIENT 3", "amount": 20 },
            { "id": 2, "date": "2021-06-04", "label": "CLIENT 15", "amount": 20 },
            { "id": 3, "date": "2021-06-10", "label": "CLIENT 16", "amount": 20 },
            { "id": 4, "date": "2021-06-11", "label": "CLIENT 80", "amount": 20 },
            { "id": 13, "date": "2021-08-30", "label": "CLIENT 1", "amount": 20 },
            { "id": 14, "date": "2021-08-28", "label": "CLIENT 46", "amount": 20 },
            { "id": 15, "date": "2021-08-01", "label": "CLIENT 1", "amount": 20 },
            { "id": 16, "date": "2021-08-19", "label": "CLIENT 45", "amount": 20 },
            { "id": 5, "date": "2021-06-01", "label": "CLIENT 69", "amount": 20 },
            { "id": 6, "date": "2021-07-10", "label": "CLIENT 86", "amount": 20 },
            { "id": 7, "date": "2021-07-01", "label": "CLIENT 22", "amount": 20 },
            { "id": 8, "date": "2021-07-15", "label": "CLIENT 55", "amount": 20 },
            { "id": 8, "date": "2021-07-15", "label": "CLIENT 55", "amount": 20 },
            { "id": 8, "date": "2021-07-15", "label": "CLIENT 55", "amount": 20 },
            { "id": 9, "date": "2021-07-01", "label": "CLIENT 66", "amount": 20 },
            { "id": 10, "date": "2021-07-01", "label": "CLIENT 4", "amount": 20 },
            { "id": 11, "date": "2021-07-01", "label": "CLIENT 2", "amount": 20 },
            { "id": 12, "date": "2021-08-01", "label": "CLIENT 1", "amount": 20 }
        ], 
    "balances": [
            { "date": "2021-06-30", "balance": 100 },
            { "date": "2021-08-30", "balance": 200 }, sans anomalie, mettre 100
            { "date": "2021-07-30", "balance": 50 } sans anomalie, mettre 120
        ]
  }
```

Bon Test à vous :)