import * as express from 'express';
import {Application, Response, Request} from 'express';
import { movementsController } from './Movements/controllers';
import * as bodyParser from 'body-parser';

const app: Application = express();

app.use(bodyParser.json());


app.get('/', (req: Request, res: Response) => {
  res.send('Hello Friend, nice to meet you, I\' m teapot');
});

app.use('/movements', movementsController);

app.listen(8080, () => {
  console.log('Server is running on http://localhost:8080');
});