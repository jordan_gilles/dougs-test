import { IBalance, IMovement, IResponse } from "./interfaces";
import {uniqWith, isEqual, orderBy } from 'lodash'

const validationService = (movements: Array<IMovement>, balances: Array<IBalance>): IResponse => {
  const response: IResponse = {statusCode: 202, message: 'Accepted', reasons: []}
  // deduplicate movements and orderer per date
  const cleanMovements: Array<IMovement>  = orderBy(uniqWith(movements, isEqual), 'date')

  // save iteration of last movement of balance period
  let saveIteration = 0
  // orderer balances per date
  orderBy(balances,'date').forEach((balance, index) => {
    const saveMovements: Array<IMovement> = []
    const saveStartDate: Date = balances[index - 1] ? balances[index - 1].date : cleanMovements[saveIteration].date 
    let sumMovements = 0

    for(let i = saveIteration; i < cleanMovements.length; ++i) {
      if(cleanMovements[i]) {
        if(cleanMovements[i].date > balance.date) {
          saveIteration = i;
          break;
        } else {
          saveMovements.push(cleanMovements[i])
          sumMovements = sumMovements + cleanMovements[i].amount
        }
      }
    }

    if(balance.balance - sumMovements !== 0) {
      const difference = sumMovements - balance.balance
      response.reasons.push({
        reason: `the sum of the movements is ${difference > 0 ? difference + ' greater' : -difference + ' less'} than the balance`,
        startDate: saveStartDate, 
        endDate: balance.date, 
        movements: saveMovements, 
        difference: difference,
        balance: balance.balance 
      })
    }
  })

  if(response.reasons.length > 0) {
    response.statusCode = 418
    response.message = "I’m a teapot"
  }

  return response
}

export default validationService;
