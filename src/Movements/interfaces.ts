export interface IMovement {
    id: number
    date: Date
    label: string
    amount: number
}
export interface IBalance {
    date: Date
    balance: number
}
export interface IReason {
    reason: string
    startDate: Date
    endDate: Date
    balance: number
    movements: Array<IMovement>
    difference: number
}
export interface IResponse {
    statusCode: number
    message: string
    reasons: Array<IReason>
  }