import { Router, Request, Response } from 'express';
import { IBalance, IMovement, IReason, IResponse } from './interfaces';
import validationService from './services';

export const movementsController =  Router();

interface IRequestBody {
  movements: Array<IMovement>
  balances: Array<IBalance>
}

movementsController.post('/validation', (req: Request, res: Response) => {
  const body: IRequestBody = req.body as IRequestBody
  const response: IResponse = validationService(body.movements, body.balances)
  return res.status(response.statusCode).json({message: response.message, reasons: response.reasons});
});